Role Name
=========

Tranning role

Requirements
------------

None

Role Variables
--------------

trainning_session_attendees: 2 # > 0

Dependencies
------------

None

Example Playbook
----------------

    - hosts: localhost
      roles:
         - { role: training }

License
-------

BSD

Author Information
------------------

Vicenç Juan Tomàs Monserrat <vicens.tomas@bluekiri.com>
